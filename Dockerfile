FROM node:16-alpine

WORKDIR /urs/src/app/

COPY backend/package*.json ./
RUN npm install

COPY backend/index.js .

COPY backend/dist/ ./dist/

EXPOSE 3000

CMD ["npm", "run", "start"]