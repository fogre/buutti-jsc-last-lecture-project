import { describe, it, expect, beforeEach } from "vitest";
import { render, screen } from "@testing-library/react";
import userEvent from '@testing-library/user-event'

import App from "../src/App.jsx";

describe("test App", () => {

    let user = null;

    beforeEach(() => {
        fetch.once(JSON.stringify([]));
        render(<App />);
        user = userEvent.setup();
    });

    it("should render H1 with text ToDo", () => {
        screen.debug();
        const header = screen.getByRole("heading", { level: 1 });
        expect(header).toHaveTextContent("ToDo");
    })

    it("should have 0 <li> elements at beginning", () => {
        const todoList = screen.queryAllByRole("listitem");
        expect(todoList).toHaveLength(0);
    })

    it("adds new todo; todo is updateable and deletable", async () => {
        const txt = "Do the dishes!"
        const input = screen.getByRole("textbox");
        const button = screen.getByRole("button");
        await user.type(input, txt);
        //Adds todo
        fetch
            .once(JSON.stringify([{ id: 1, item: txt, isDone: false }]));
        await user.click(button);
        let todoList = screen.queryAllByRole("listitem");
        expect(todoList).toHaveLength(1);
        const addedItem = todoList[0];
        expect(addedItem).toHaveTextContent(txt);
        const itemSpanElement = addedItem.children[0];
        expect(itemSpanElement).not.toHaveClass("done");
        //Updates todo
        fetch
            .once(JSON.stringify([{ id: 1, item: txt, isDone: true }]));
        await user.click(itemSpanElement);
        expect(itemSpanElement).toHaveClass("done");
        //Removes todo
        fetch
            .once(JSON.stringify([]));
        const itemDeleteIcon = addedItem.children[1];
        await user.click(itemDeleteIcon);
        todoList = screen.queryAllByRole("listitem");
        expect(todoList).toHaveLength(0);
    })
})




