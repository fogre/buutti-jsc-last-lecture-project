import { useState, useEffect } from 'react'
import { BiTrash } from 'react-icons/bi'
import './App.css'

let uniqueId = 100

const App = () => {
    const [list, setList] = useState([])
    const [input, setInput] = useState('')

    useEffect(() => {
        fetchTodos()
    }, []);

    const fetchTodos = async () => {
        const res = await fetch("/api/todos")
        const asJson = await res.json();
        setList(asJson)
    }

    const addItem = async () => {
        if (!input) return null
        const newTodo = { item: input }
        const res = await fetch("/api/todos", {
            method: "POST",
            headers: { 'Content-Type': 'application/json' },    
            body: JSON.stringify(newTodo) 
        });
        const asJson = await res.json();
        
        setList(asJson);
        setInput('')
    }

    const toggle = (id) => async () => {
        const res = await fetch(`/api/todos/${id}`, {
            method: "PUT"
        });
        const asJson = await res.json();
        setList(asJson);
    }

    const remove = (id) => async () => {
        const res = await fetch(`/api/todos/${id}`, {
            method: "DELETE"
        });
        const asJson = await res.json();
        setList(asJson);
    }

    return <div className='App'>
        <h1>ToDo</h1>
        <input value={input} onChange={(event) => setInput(event.target.value)} />
        <button onClick={addItem}>Add</button>
        <ul>
            {list.map(({ id, isDone, item }) => {
                return <li key={id} className='item'>
                    <span className={isDone ? 'done' : ''}
                        onClick={toggle(id)}>
                        {item}
                    </span>
                    <BiTrash className='icon' onClick={remove(id)} />
                </li>
            })}
        </ul>

    </div>
}

export default App