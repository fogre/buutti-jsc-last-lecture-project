import express from "express";

let todoId = 0;
let todos = [];

const app = express();
app.use(express.json())
app.use("/", express.static("./dist"))

app.get("/api/todos", (_reg, res) => {
  res.send(todos);
});

app.post("/api/todos", (req, res) => {
  const { item } = req.body;
  
  if (!item) {
    return res.status(400).send({ error: "Missing required body parameter item" });
  }

  const newTodo = {
    item,
    id: todoId++,
    isDone: false
  }
  todos.push(newTodo);
  res.send(todos);
});

app.delete("/api/todos/:id", (req, res) => {
  const id = req.params.id;
  todos = todos.filter(t => t.id != id);
  res.send(todos);
});

app.put("/api/todos/:id", (req, res) => {
  const id = req.params.id;
  todos = todos.map(t => {
    if (t.id == id) {
      t.isDone = !t.isDone
    }
    return t;
  });
  res.send(todos);
});

app.get("/api/version", (_req, res) => {
  res.send("1.0.2")
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log("Listening port: ", PORT);
});

export default app;